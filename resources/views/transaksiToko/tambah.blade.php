@extends('layouts.layoutToko')  

@section('content')
  <!-- Horizontal Form -->
          <section class="content">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah Transaksi</h3>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" action="{{ url('/tambah_transaksiToko')}}">

              {{ csrf_field() }}
              
              <div class="box-body">
                <div class="form-group">
                  <label for="text" >Nama Pembeli</label>
                  <div >
                    <input type="text" class="form-control" placeholder="Masukkan Nama Pembeli" name="nama_pembeli">
                    {!! $errors->first('nama_pembeli', '<strong class="text-danger">:message</strong>') !!}
                  </div>
                 </div>
                 <div>
              
                
                
                <div class="form-group field_wrapper">
                    <select class="s-items" name="barang[]"><option value="">Item</option>@foreach($barang as $transaksi)<option value="{{ $transaksi->id }}">{{ $transaksi->nama_barang}}</option>@endforeach</select>
                   <a href="javascript:void(0);" class="add_button btn btn-info btn-xs disabled" title="Add field" disabled="disabled">Add items</a>
                   
                <br/>
                  <!-- <div>
                    <select class="s-items" required name="transaksi[]">
                    <option value="">Item</option>
                    @foreach($barang as $transaksi)
                    <option value="{{ $transaksi->id }}">{{ $transaksi->nama_barang}}</option>
                    @endforeach
                    </select>
                    {!! $errors->first('transaksi[]', '<strong class="text-danger">:message</strong>') !!}
                    <input type="number" placeholder="Masukkan Jumlah" required name="field_jumlah[]" value=""/>
                    {!! $errors->first('field_jumlah[]', '<strong class="text-danger">:message</strong>') !!}
                   
                  </div> -->         
               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          </section>

@endsection
@section('script')
 <script>
$(document).ready(function(){
  var maxField = 999; //Input fields increment limitation
  var addButton = $('.add_button'); //Add button selector
  var wrapper = $('.field_wrapper'); //Input field wrapper
  fieldHTML = null;
  // console.log(option);
  $('.s-items').on('change', function() {
      var id = $('.s-items').find('option:selected').val();
      var name = $('.s-items').find('option:selected').text();
      // console.log([id,name]);
      // var text = 
      // $.each(selected, function(index,val) {
          fieldHTML = '<div><input type="hidden" name="transaksi[]" value="'+id+'" id="transaksi_'+id+'"><input disabled value="'+name+'">&nbsp;<input type="number"  placeholder="Masukkan Jumlah" name="field_jumlah[]" min="0" value="" required/>&nbsp;<a href="javascript:void(0);" class="remove_button btn btn-danger btn-sm" title="Remove field"><i class="fa fa-trash"></i></a></div>';  
      // })
      $('.s-items').find('option:selected').addClass('disabled');
      $('.s-items').find('option:selected').attr('disabled', 'disabled');
      // wrapper.append(fieldHTML);
      // console.log(fieldHTML);
      addButton.removeAttr('disabled');
      addButton.removeClass('disabled');

      
  });
  
  // var fieldHTML = '<div><select class="s-items" name="transaksi[]"><option value="">Item</option>@foreach($barang as $transaksi)<option value="{{ $transaksi->id }}">{{ $transaksi->nama_barang}}</option>@endforeach</select>&nbsp;<input type="number"  placeholder="Masukkan Jumlah" name="field_jumlah[]" value="" required/>&nbsp;<a href="javascript:void(0);" class="remove_button btn btn-danger btn-sm" title="Remove field"><i class="fa fa-trash"></i></a></div>'; //New input field html

  var x = 1; //Initial field counter is 1

  $(addButton).click(function(){ //Once add button is clicked
    if(x < maxField){ //Check maximum number of input fields
      x++; //Increment field counter
      wrapper.append(fieldHTML); // Add field html
      addButton.attr('disabled', 'disabled');
      addButton.addClass('disabled');
          }
  });
  $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
    e.preventDefault();
    var id = $(this).siblings('input[type=hidden]').val();
    // console.log(id);
    $('.s-items').find('option[value="'+id+'"]').removeAttr('disabled');
    $('.s-items').find('option[value="'+id+'"]').removeClass('disabled');
    $(this).parent('div').remove(); //Remove field html
    x--; //Decrement field counter
  });
});
</script>
@endsection
