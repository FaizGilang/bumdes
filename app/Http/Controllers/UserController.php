<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PetugasTokoRequest;
use App\Http\Requests\PetugasBumdes;
use App\Http\Requests\ProfilBumdes;
use App\Http\Requests\ProfilAdmin;
use App\Http\Requests\ProfilToko;
use App\Http\Requests\EditPetugasToko;
use App\Http\Requests\EditPetugasBumdes;
use App\Transaksi;
use App\User;
use Auth;
use App\Toko;
use App\TokoDetail;
use Alert;

class UserController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['addPetugas', 'savePetugas', 'index', 'editPetugas', 'updatePetugas', 'deletePetugas', 'editProfilAdmin', 'updateProfilAdmin']]);
        $this->middleware('bumdes', ['only' => ['addPetugasToko', 'savePetugasToko', 'indexPetugasToko', 'editPetugasToko', 'updatePetugasToko', 'deletePetugasToko', 'editProfilBumdes','updateProfilBumdes']]);
        $this->middleware('toko', ['only' => ['editProfilToko', 'updateProfilToko']]);
        $this->middleware('admindantoko', ['only' => ['resetPassword']]);
        
    }
    //Fungsi memanggil form tambah petugas
    public function addPetugas()
    {
    	return view ('user.tambah');
    }
    //Fungsi menyimpan tambah petuas
    public function savePetugas(PetugasBumdes $request)
    {
       


        $user= new User;
        
        $user->name = $request->name;
        $user->role = "petugasbumdes";
        $user->email = $request->email;
        $user->password = bcrypt ($request->password);

        $user->save();
        Alert::success('Petugas Bumdes berhasil ditambah', 'Sukses');
        return redirect('/tampil_petugas');

    }
    //Fungsi untuk menampilkan
    public function index()
    {
    	$data=User::where('role','=','petugasbumdes')->get();
    	return view ('user.tampil')->with('user',$data);
    }

    public function editPetugas($id)
    {
        $data= User::find($id);
        return view ('user.edit')->with('user',$data);
    }

    
    public function updatePetugas (EditPetugasBumdes $request,$id)
    {

        $user=User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        
        

        $user->save();
        Alert::success('Petugas Bumdes berhasil diubah', 'Sukses');
        return redirect('/tampil_petugas');

    }

    public function deletePetugas($id)
    {
        $user= User::findOrFail($id);
        
     
        $check= Transaksi::where('id_petugasBumdes','=',$id)->count();
        if ($check>0){
            Alert::warning('Petugas Bumdes tidak dapat dihapus karena petugas sudah memiliki data transaksi', 'Sorry');
        }
        else {
            $user -> delete();
            Alert::success('Petugas Bumdes berhasil dihapus', 'Sukses');
        }
        return redirect('/tampil_petugas');
    }
    public function addPetugasToko()
    {
        $data=Toko::all();
        return view ('petugasToko.tambah')->with('toko',$data);
    }
    //Fungsi menyimpan tambah petugas
    public function savePetugasToko(PetugasTokoRequest $request)
    {

        $user= new User;
        
        $user->name = $request->name;
        $user->role = "petugastoko";
        $user->email = $request->email;
        $user->password= bcrypt ($request->password);

        $user->save();
        Alert::success('Petugas Toko berhasil ditambah', 'Sukses');
        return redirect('/tampil_petugasToko');

    }
    //Fungsi untuk menampilkan
    public function indexPetugasToko()
    {
        $data=User::where('role','=','petugastoko')->get();
        return view ('petugasToko.tampil')->with('user',$data);
    }

    public function editPetugasToko($id)
    {
        $data=User::find($id);
        return view ('petugasToko.edit')->with('user',$data);
    }

    
    public function updatePetugasToko (EditPetugasToko $request,$id)
    {
        
        $user=User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        

        $user->save();
        Alert::success('Petugas Toko berhasil diubah', 'Sukses');
        return redirect('/tampil_petugasToko');

    }

    public function deletePetugasToko($id)
    {
        $user= User::findOrFail($id);

        $check= Transaksi::where('id_petugasToko','=',$id)->count();
        if ($check>0){
            Alert::warning('Petugas Toko tidak dapat dihapus karena petugas sudah memiliki data transaksi', 'Sorry');
        }
        else {
            $data= Toko::where('pemilik_toko','=',$id)
            ->count();
            $user -> delete();
                if($data > 0){
                 $del= Toko::where('pemilik_toko','=',$id)->get()->first();
                 $del-> delete();
                }
            Alert::success('Petugas Toko berhasil dihapus', 'Sukses');
        }


        return redirect('/tampil_petugasToko');
    }
   
    public function resetPassword($id)
    {
        $data =User::find($id);
        $data->password=bcrypt('passwordbaru');
        $data->save();
        if($data->role == 'petugasbumdes'){
            Alert::success('Password telah berhasil direset, Password : passwordbaru', 'Sukses');
             return redirect('/tampil_petugas');
        } else {
            Alert::success('Password telah berhasil direset, Password : passwordbaru', 'Sukses');
             return redirect('/tampil_petugasToko');
         }
    }
    public function editProfilBumdes($id)
    {
        $data= User::find($id);
        return view ('user.profilBumdes')->with('user',$data);
    }
    public function updateProfilBumdes(ProfilBumdes $request,$id)
    {
        $data= User::find($id);

        $data->name = $request->name;
        $data->email = $request->email;
        if($request->password == ''){
        }
        else {
            $data->password =  bcrypt ($request->password);
        }
        $data->save();
        Alert::success('Profil berhasil diubah', 'Sukses');
        return redirect('tampil_dashboardBumdes');
    }
     public function editProfilToko($id)
    {
        $data= User::find($id);
        return view ('user.profilToko')->with('user',$data);
    }
    public function updateProfilToko(ProfilToko $request,$id)
    {
        $data= User::find($id);

        $data->name = $request->name;
        $data->email = $request->email;
        if($request->password == ''){
        }
        else{
            $data->password = bcrypt ($request->password);
        }
        $data->save();
        Alert::success('Profil berhasil diubah', 'Sukses');
        return redirect('tampil_dashboardToko');
    }
    public function editProfilAdmin($id)
    {
        $data= User::find($id);
        return view ('user.profilAdmin')->with('user',$data);
    }
    public function updateProfilAdmin(ProfilAdmin $request,$id)
    {
        $data= User::find($id);

        $data->name = $request->name;
        $data->email = $request->email;
        if($request->password == ''){
        }
        else{
            $data->password = bcrypt ($request->password);
        }
        $data->save();
        Alert::success('Profil berhasil diubah', 'Sukses');
        return redirect('tampil_dashboardAdmin');
    }
    
   
   

}
