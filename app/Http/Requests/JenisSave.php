<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JenisSave extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jenis' => 'required',
            'jenis' => 'unique:jenis',
        ];
    }
    public function messages()
    {
        return[
        'jenis.required' => 'Jenis tidak boleh kosong',
        'jenis.unique' => 'Jenis harus unik',
        ];
    }
}
