<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfilToko extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'email|unique:users,email,'.$this->id,
            'password' => 'min:6|confirmed',
        ];
    }
    public function messages()
    {
        return[
        'name.required' => 'Nama tidak boleh kosong',
        'email.unique' => 'Email harus unik',
        'password.min' => 'Password harus lebih dari 6 karakter',
        'password.confirmed' => 'Password tidak sama',
        ];
    }
}
