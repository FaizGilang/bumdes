<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BarangSave extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'nama_barang' =>'required', 
        'jenis' => 'required',
        'satuan' => 'required',
        'harga_beli' => 'required|integer|between:1,10000000',
        'harga' => 'required|integer|between:1,10000000',
        'stok' => 'required|integer|between:1,10000000',
        ];
    }

    public function messages()
    {
        return[
        'nama_barang.required' =>'Nama tidak boleh kosong',
        'jenis.required' =>'Jenis tidak boleh kosong',
        'satuan.required' => 'Satuan tidak boleh kosong',
        'harga_beli.required' => 'Harga Beli tidak boleh kosong',
        'harga_beli.between' => 'Harga Beli tidak boleh negatif',
        'harga_beli.integer' => 'ra ono tititke ndes',
        'harga.required' => 'Harga Jual tidak boleh kosong',
        'harga.between' => 'Harga Jual tidak boleh negatif',
        'stok.required' => 'Stok tidak boleh kosong',
        'stok.between' => 'Stok tidak boleh negatif',
        ];
    }
}
