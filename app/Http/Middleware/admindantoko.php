<?php

namespace App\Http\Middleware;

use Closure;

class admindantoko
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->role != 'admin' && auth()->user()->role != 'petugasbumdes') {
            return redirect('/');
        }
        return $next($request);
    }
}
