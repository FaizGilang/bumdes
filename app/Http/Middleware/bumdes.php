<?php

namespace App\Http\Middleware;

use Closure;

class bumdes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->role != 'petugasbumdes') {
            return redirect('/');
        }
        return $next($request);
    }
}
